#ifndef _SMG_H
#define _SMG_H

#include "Sys.h"

extern u8 code SMG[];
extern u8 SMG_Buff[8];

void Refresh();
void SMG_Display();

#endif